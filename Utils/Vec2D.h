/** Vec2D.h
*  Created by: Arvand Kaveh
*  Created on: 10/31/2020 3:49:49 PM
* Description: 
*/

#pragma once
#include <iostream>

class Vec2D
{
	public:

	// This will use the default constructor which results in a zero vector
	static const Vec2D Zero;

	// Constructors
	Vec2D(): Vec2D(0,0){}
	Vec2D(float x, float y) : mX{ x }, mY{ y }{}
	
	// Setters and Getters
	inline float GetX() const { return mX; }
	inline float GetY() const { return mY; }
	inline void SetX(float x) { mX = x; }
	inline void SetY(float y) { mY = y; }

	bool operator==(const Vec2D& vec) const;
	bool operator!=(const Vec2D& vec) const;

	Vec2D operator-() const;
	Vec2D operator*(float scalar) const;
	Vec2D operator/(float scalar) const;

	Vec2D& operator*=(float scalar);
	Vec2D& operator/=(float scalar);

	Vec2D operator+(const Vec2D& vec) const;
	Vec2D operator-(const Vec2D& vec) const;
	Vec2D& operator+=(const Vec2D& vec);
	Vec2D& operator-=(const Vec2D& vec);

	float Mag2() const;
	float Mag() const;

	Vec2D GetUnitVec() const;
	Vec2D& Normalize();

	float Distance(const Vec2D& vec) const;
	float Dot(const Vec2D& vec) const;

	Vec2D ProjectOnto(const Vec2D& vec) const;

	float AngleBetween(const Vec2D& vec2) const;

	Vec2D Reflect(const Vec2D& normal) const;

	void Rotate(float angle, const Vec2D& aroundPoint);
	Vec2D RotationResult(float angle, const Vec2D& aroundPoint) const;

	// In order to add commutative property to our operator *
	friend Vec2D operator*(float scalar, const Vec2D& vec);

	friend std::ostream& operator<<(std::ostream out, const Vec2D v);

private:
	float mX;
	float mY;
};
