/*
* Utils.h
*  Created by: Arvand Kaveh
*  Created on: 11/7/2020 4:15:46 PM
* Description: 
*/

#pragma once

static const float EPSILON{ 0.0001f };

const float PI{ 3.141592f };
const float TWO_PI{ 2.0f * PI};

bool IsEqual(float a, float b);
bool IsGreaterThanOrEqual(float a, float b);
bool IsSmallerThanOrEqual(float a, float b);
