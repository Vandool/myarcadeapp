/*
* Utils.cpp
*  Created by: Arvand Kaveh
*  Created on: 11/7/2020 4:25:27 PM
* Description:
*/

#pragma once
#include "Utils.h"
#include <cmath>


bool IsEqual(float a, float b)
{
	return std::abs(a - b) < EPSILON;
}

bool IsGreaterThanOrEqual(float a, float b)
{
	return a > b || IsEqual(a, b);
}

bool IsSmallerThanOrEqual(float a, float b)
{
	return a < b || IsEqual(a, b);
}