/*
* Screen.cpp
*  Created by: Arvand Kaveh
*  Created on: 11/24/2020 9:11:29 PM
* Description:
*/

#pragma once
#include "Screen.h"
#include "Vec2D.h"
#include "Utils.h"
#include <SDL.h>
#include <cassert>

Screen::Screen():mWidth(0), mHeight(0), moptrWindow(nullptr), mnoptrWindowSurface(nullptr)
{

}

Screen::~Screen()
{
	if (moptrWindow)
	{
		// Clean up and quit
		SDL_DestroyWindow(moptrWindow);
		moptrWindow = nullptr;
	}

	SDL_Quit();
}


SDL_Window* Screen::Init(uint32_t w, uint32_t h, uint32_t mag)
{

	if (SDL_Init(SDL_INIT_EVERYTHING))
	{
		std::cout << "Error SDL_Init Failed" << "\n";

		// because we're returning a pointer, all our errors should retrun nullptr
		return nullptr;
	}

	mWidth = w;
	mHeight = h;

	// in SDL wiki, API by Category are all the fucntions 
	// Create Window for SDL
	moptrWindow = SDL_CreateWindow("Arcade", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, mWidth, mHeight, 0);

	if (moptrWindow)
	{
		// Get the Surface
		// SDL_Surface is in short a 2D Array of pixels
		// This is effectively the Canvas on which we draw
		mnoptrWindowSurface = SDL_GetWindowSurface(moptrWindow);

		// Initialise Colors
		SDL_PixelFormat * pixelFormat = mnoptrWindowSurface->format;

		// There are plenty of color formats, it can be different by default in different systems
		// That's why we need to make a generic class
		// We need to initialze pixelformat in order to use any color
		Color::InitColorFormat(pixelFormat);
		mClearColor = Color::Black();

		mBackBuffer.Init(pixelFormat->format, mWidth, mHeight);

		mBackBuffer.Clear(mClearColor);
	}
}

void Screen::SwapScreens()
{
	assert(moptrWindow);
	if (moptrWindow)
	{
		// Clears the front facing screen
		ClearScreen();

		// Copies conents from our BackBuffer to front-facing buffer
		SDL_BlitSurface(mBackBuffer.GetSurface(), nullptr, mnoptrWindowSurface, nullptr);

		// Copy the Window surface to the Screen
		SDL_UpdateWindowSurface(moptrWindow);

		mBackBuffer.Clear(mClearColor);
	}
}

// Draw methods
void Screen::Draw(int x, int y, const Color& color)
{
	assert(moptrWindow);
	if (moptrWindow)
	{
		mBackBuffer.SetPixel(color, x, y);
	}
}

void Screen::Draw(const Vec2D& point, const Color& color)
{
	assert(moptrWindow);
	if (moptrWindow)
	{
		mBackBuffer.SetPixel(color, point.GetX(), point.GetY());
	}
}

void Screen::ClearScreen()
{
	assert(moptrWindow);
	if (moptrWindow)
	{
		SDL_FillRect(mnoptrWindowSurface, nullptr, mClearColor.GetPixelColor());
	}
}