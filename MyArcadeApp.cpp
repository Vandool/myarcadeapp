#include <SDL.h>
#undef main

#include <iostream>
#include <Color.h>
#include <ScreenBuffer.h>
#include <Screen.h>

const int SCREEN_WIDTH = 224;
const int SCREEN_HEIGHT = 288;

int main(int argc, const char * argv[])
{

	Screen theScreen;

	theScreen.Init(SCREEN_WIDTH, SCREEN_HEIGHT, 1);
	theScreen.Draw(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, Color::Yellow());
	theScreen.SwapScreens();


	SDL_Event sdlEvent;
	bool runnig{ true };

	while (runnig)
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			switch (sdlEvent.type)
			{
			case SDL_QUIT:
				runnig = false;
				break;
			}
		}
	}

	return 0;
}
